<?php

namespace Mbs\BackendController\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{
    protected $_template = 'Mbs_BackendController::system/config/button.phtml';
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->urlBuilder = $urlBuilder;
    }

    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getCustomUrl()
    {
        return $this->urlBuilder->getUrl('jsoninadmin/index/jsoncall', [ 'key' => $this->urlBuilder->getSecretKey('jsoninadmin','index','jsoncall')]);
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setData(
            [
                'id' => 'btn_id',
                'label' => __('Button Click me'),
                'onclick' => 'setLocation(\'' . $this->getCustomUrl() . '\')',
            ]
        );
        return $button->toHtml();
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}