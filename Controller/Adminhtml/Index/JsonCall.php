<?php

namespace Mbs\BackendController\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;

class JsonCall extends \Magento\Backend\App\Action
    implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
    }

    protected function _isAllowed()
    {
        return true;
    }

    public function execute()
    {
        $data = ['test'=> 'this is the response'];

        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }
}